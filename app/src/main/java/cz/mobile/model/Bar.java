package cz.mobile.model;


import java.sql.Date;


public class Bar {

    private long id;
    private String name;
    private String type;
    private String city;
    private String street;
    private String text;
    private String beer;
    private boolean kitchen;
    private boolean garden;

    public Bar(long id, String name, String type, String city, String street, String text, String beer, boolean kitchen, boolean garden) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.city = city;
        this.street = street;
        this.text = text;
        this.beer = beer;
        this.kitchen = kitchen;
        this.garden = garden;
    }

    public String getBeer() {
        return beer;
    }

    public void setBeer(String beer) {
        this.beer = beer;
    }

    public boolean getKitchen() {
        return kitchen;
    }

    public void setKitchen(boolean kitchen) {
        this.kitchen = kitchen;
    }

    public boolean getGarden() {
        return garden;
    }

    public void setGarden(boolean garden) {
        this.garden = garden;
    }

    public Bar(){

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}