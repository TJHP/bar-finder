package cz.mobile.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import cz.mobile.R;
import cz.mobile.model.Review;
import cz.mobile.model.Reviewer;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener authListener;
    private StorageReference storage;
    private TextView name;
    public Uri imguri;
    private ImageView img;
    private StorageTask uploadTesk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img = (ImageView) findViewById(R.id.imageView);
        name = (TextView) findViewById(R.id.nameMain);
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final String userid = user.getUid();

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        String realn = dataSnapshot.child("Reviewer").child(userid).child("realname").getValue(String.class);
                        name.setText(realn);
                    } else {
                        name.setText("Nepřihlášený uživatel");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }

        Button btn = (Button) findViewById(R.id.registButton);
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            btn.setVisibility(View.GONE);
        } else {
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, RegistrationActivity.class));
                }
            });
        }


        Button btnSats = (Button) findViewById(R.id.statsButton);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            btnSats.setVisibility(View.GONE);
        } else {
            btnSats.setVisibility(View.VISIBLE);
            btnSats.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, StatsActiviy.class));
                }
            });
        }

        Button logout = (Button) findViewById(R.id.logout);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            logout.setVisibility(View.GONE);
        } else {
            logout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth.getInstance().signOut();
                    startActivity(new Intent(MainActivity.this, MainActivity.class));
                }
            });
        }

        Button login = (Button) findViewById(R.id.loginButtonMain);
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            login.setVisibility(View.GONE);
        } else {
            login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            });
        }

        Button sett = (Button) findViewById(R.id.settings);
        sett.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

        Button favs = (Button) findViewById(R.id.favourites);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            favs.setVisibility(View.GONE);
        } else {
            favs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, FavouritesActivity.class));
                }
            });
        }

        Button find = (Button) findViewById(R.id.find);
        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FindbynameActivity.class));
            }
        });


        Button manuall = (Button) findViewById(R.id.manually);
        manuall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ManualfindActivity.class));
            }
        });


        Button maps = (Button) findViewById(R.id.maps);
        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MapsActivity.class));
            }
        });


        Button addbar = (Button) findViewById(R.id.addbar);
        if (FirebaseAuth.getInstance().getCurrentUser() == null) {
            addbar.setVisibility(View.GONE);
        } else {
            addbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, AddnewbarActivity.class));
                }
            });
        }

        Button imgset = (Button) findViewById(R.id.imgset);
        imgset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FileChooser();
            }
        });

        storage = FirebaseStorage.getInstance().getReference("Images");
        Button imgadd = (Button) findViewById(R.id.imgadd);
        imgadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (uploadTesk != null && uploadTesk.isInProgress()) {
                    Toast.makeText(MainActivity.this, "Uploading", Toast.LENGTH_LONG).show();
                } else {
                    FileUploader();
                }
            }
        });
    }

    private String getExtension(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(cr.getType(uri));
    }

    public void FileUploader() {
        StorageReference ref = storage.child(System.currentTimeMillis() + "." + getExtension(imguri));

        uploadTesk = storage.putFile(imguri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(MainActivity.this, "Image cannot be uploaded", Toast.LENGTH_LONG).show();
                    }
                });
        img.setImageURI(imguri);
    }

    public void FileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imguri = data.getData();
        }
    }
}
