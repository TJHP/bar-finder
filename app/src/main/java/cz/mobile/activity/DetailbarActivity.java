package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import cz.mobile.R;
import cz.mobile.model.Bar;


public class DetailbarActivity extends AppCompatActivity {

    private TextView name, beer, city, street, kitchen, garden, type, text;
    private String findingName;
    private DatabaseReference reff, refftwo;
    private Button addfav;
    private String firename, firetype, firebeer, firecity, firestreet, firetext, firekitchen, firegarden;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailbar);

        name = (TextView) findViewById(R.id.detname);
        beer = (TextView) findViewById(R.id.detbeer);
        city = (TextView) findViewById(R.id.detcity);
        street = (TextView) findViewById(R.id.detstreet);
        kitchen = (TextView) findViewById(R.id.detkitchen);
        garden = (TextView) findViewById(R.id.detgarden);
        type = (TextView) findViewById(R.id.dettype);
        text = (TextView) findViewById(R.id.dettext);

        findingName = getIntent().getStringExtra("barname");
        reff = FirebaseDatabase.getInstance().getReference().child("Bar").child(findingName);
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                firename = dataSnapshot.child("name").getValue().toString();
                firetype = dataSnapshot.child("type").getValue().toString();
                firebeer = dataSnapshot.child("beer").getValue().toString();
                firecity = dataSnapshot.child("city").getValue().toString();
                firestreet = dataSnapshot.child("street").getValue().toString();
                firetext = dataSnapshot.child("text").getValue().toString();
                firekitchen = dataSnapshot.child("kitchen").getValue().toString();
                firegarden = dataSnapshot.child("garden").getValue().toString();

                if (firekitchen.equals("true")) {
                    firekitchen = "Ano";
                } else {
                    firekitchen = "Ne";
                }

                if (firegarden.equals("true")) {
                    firegarden = "Ano";
                } else {
                    firegarden = "Ne";
                }

                name.setText(firename);
                type.setText(firetype);
                beer.setText(firebeer);
                city.setText(firecity);
                street.setText(firestreet);
                text.setText(firetext);
                kitchen.setText(firekitchen);
                garden.setText(firegarden);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        addfav = (Button) findViewById(R.id.addfav);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            addfav.setVisibility(View.VISIBLE);
            addfav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    refftwo = FirebaseDatabase.getInstance().getReference().child("Favourites").child(uid);
                    Bar bar = new Bar();
                    bar.setName(firename);
                    bar.setBeer(firebeer);
                    bar.setStreet(firestreet);
                    bar.setType(firetype);
                    bar.setText(firetext);
                    bar.setCity(firecity);

                    boolean kitch, gard;
                    if (firekitchen.equals("true")) {
                        kitch = true;
                    } else {
                        kitch = false;
                    }

                    if (firegarden.equals("true")) {
                        gard = true;
                    } else {
                        gard = false;
                    }

                    bar.setKitchen(kitch);
                    bar.setGarden(gard);

                    refftwo.child(name.getText().toString()).setValue(bar);
                    Toast.makeText(DetailbarActivity.this, "Podnik přidán", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            addfav.setVisibility(View.GONE);
        }
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}