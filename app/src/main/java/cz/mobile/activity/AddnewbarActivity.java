package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cz.mobile.R;
import cz.mobile.model.Bar;

public class AddnewbarActivity extends AppCompatActivity {

    private DatabaseReference reff;
    private EditText inpname, inptype, inpcity, inpstreet, inptext;
    private CheckBox kitchen, garden;
    private Button add;
    private long id = 0;
    private Bar bar;
    private Spinner dropdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addnewbar);

        inpname = (EditText) findViewById(R.id.impname);
        inptype = (EditText) findViewById(R.id.imptype);
        inpcity = (EditText) findViewById(R.id.impcity);
        inpstreet = (EditText) findViewById(R.id.impstreet);
        inptext = (EditText) findViewById(R.id.imptext);
        kitchen = (CheckBox) findViewById(R.id.kitchen);
        garden = (CheckBox) findViewById(R.id.garden);

        bar = new Bar();
        reff = FirebaseDatabase.getInstance().getReference().child("Bar");
        add = (Button) findViewById(R.id.addnewb);

        dropdown = findViewById(R.id.typespinner);
        String[] items = new String[]{"Bar", "Nálevna", "Restaurace"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);


        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    id = (dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = inpname.getText().toString().trim();
                String type = dropdown.getSelectedItem().toString();
                String beer = inptype.getText().toString().trim();
                String city = inpcity.getText().toString().trim();
                String street = inpstreet.getText().toString().trim();
                String text = inptext.getText().toString().trim();
                boolean kitch;
                boolean gard;
                if (kitchen.isChecked()) {
                    kitch = true;
                } else {
                    kitch = false;
                }

                if (garden.isChecked()) {
                    gard = true;
                } else {
                    gard = false;
                }

                if (name.isEmpty() || name.length() < 5) {
                    inpname.setError("Název je vyžadován! Minimum 5 znaků.");
                    inpname.requestFocus();
                }
                if (type.isEmpty() || type.length() < 3) {
                    inptype.setError("Typ je vyžadován! Minimum 3 znaky.");
                    inptype.requestFocus();
                }
                if (city.isEmpty() || city.length() < 2) {
                    inpcity.setError("Město je vyžadováno!");
                    inpcity.requestFocus();
                    inpcity.requestFocus();
                }
                if (street.isEmpty() || street.length() < 5) {
                    inpstreet.setError("Název ulice je vyžadován!");
                    inpstreet.requestFocus();
                }

                bar.setName(name);
                bar.setId(id + 1);
                bar.setType(type);
                bar.setBeer(beer);
                bar.setGarden(gard);
                bar.setKitchen(kitch);
                bar.setCity(city);
                bar.setStreet(street);
                bar.setText(text);

                reff.child(String.valueOf(name)).setValue(bar);
                Toast.makeText(AddnewbarActivity.this, "Nový podnik přidán!", Toast.LENGTH_LONG).show();
                startActivity(new Intent(AddnewbarActivity.this, MainActivity.class));
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
