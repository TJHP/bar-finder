package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import cz.mobile.R;
import cz.mobile.model.Bar;

public class FavouritesActivity extends AppCompatActivity {

    private ListView favouritesList;
    private ArrayList<String> barnames;
    private ArrayList<Bar> bars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);

        favouritesList = (ListView) findViewById(R.id.favouritesList);

        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            final String userid = user.getUid();

            DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Favourites").child(userid);
            reference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        bars = new ArrayList<>();
                        barnames = new ArrayList<>();
                        for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                            bars.add(userSnapshot.getValue(Bar.class));
                            barnames.add(userSnapshot.getValue(Bar.class).getName().toString());
                        }
                        ArrayAdapter adapter = new ArrayAdapter(FavouritesActivity.this, android.R.layout.simple_list_item_1, barnames);
                        favouritesList.setAdapter(adapter);
                    } else {
                        Toast.makeText(FavouritesActivity.this, "Žádné oblíbené podniky", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                }
            });
        }
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        favouritesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = barnames.get(position).toString();
                Intent intent = new Intent(getBaseContext(), DetailbarActivity.class);
                intent.putExtra("barname", name);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
