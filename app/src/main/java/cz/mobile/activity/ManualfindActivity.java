package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cz.mobile.R;

public class ManualfindActivity extends AppCompatActivity {

    private DatabaseReference reff;
    private Button find;
    private EditText name;
    private String barname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manualfind);

        find = (Button) findViewById(R.id.search);
        name = (EditText) findViewById(R.id.namemanuall);

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barname = name.getText().toString();
                reff = FirebaseDatabase.getInstance().getReference().child("Bar").child(barname);
                reff.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Intent intent = new Intent(getBaseContext(), DetailbarActivity.class);
                            intent.putExtra("barname", barname);
                            startActivity(intent);
                        } else {
                            Toast.makeText(ManualfindActivity.this, "Bar is not found", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
