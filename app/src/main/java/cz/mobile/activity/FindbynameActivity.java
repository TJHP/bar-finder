package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cz.mobile.R;

public class FindbynameActivity extends AppCompatActivity {

    private Button find;
    private EditText name;
    private String barname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findbyname);

        find = (Button) findViewById(R.id.searchbname);
        name = (EditText) findViewById(R.id.citybname);

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barname = name.getText().toString();
                Intent intent = new Intent(getBaseContext(), NearbarsActivity.class);
                intent.putExtra("barname", barname);
                startActivity(intent);
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
