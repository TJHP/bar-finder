package cz.mobile.activity;


import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import cz.mobile.R;
import cz.mobile.model.Reviewer;


public class RegistrationActivity extends AppCompatActivity {

    private EditText username, password, realname;
    private String name, pass, realn;
    private Button regButton, logButton;
    private DatabaseReference reff;
    private FirebaseAuth auth;
    private Reviewer reviewer;
    private long id = 0;
    private int rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        username = (EditText) findViewById(R.id.registUsername);
        password = (EditText) findViewById(R.id.registPass);
        realname = (EditText) findViewById((R.id.nameRegist));
        regButton = (Button) findViewById(R.id.registrate);

        reviewer = new Reviewer();
        reff = FirebaseDatabase.getInstance().getReference().child("Reviewer");

        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    id = (dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        auth = FirebaseAuth.getInstance();
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = username.getText().toString().trim();
                pass = password.getText().toString().trim();
                realn = realname.getText().toString().trim();
                rating = 100;

                if (name.isEmpty()) {
                    username.setError("Correct email is required!");
                    username.requestFocus();
                } else if (pass.isEmpty()) {
                    password.setError("Strong password is required!");
                    password.requestFocus();
                } else if (name.isEmpty() && pass.isEmpty()) {
                    Toast.makeText(RegistrationActivity.this, "Field are empty!", Toast.LENGTH_LONG).show();
                } else {
                    auth.createUserWithEmailAndPassword(name, pass).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                Toast.makeText(RegistrationActivity.this, "Something went wrong! Please try again.", Toast.LENGTH_LONG).show();
                            } else {
                                reviewer.setId(id + 1);
                                reviewer.setUsername(name);
                                reviewer.setPassword(pass);
                                reviewer.setRealname(realn);
                                reviewer.setRating(rating);
                               // reff.child(String.valueOf(id + 1)).setValue(reviewer);
                                reff.child(String.valueOf(auth.getCurrentUser().getUid())).setValue(reviewer);

                                Toast.makeText(RegistrationActivity.this, "Registration successful!", Toast.LENGTH_LONG).show();
                                startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
                            }
                        }
                    });
                }
            }
        });

        logButton = (Button) findViewById(R.id.loginReg);

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
