package cz.mobile.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import cz.mobile.R;
import cz.mobile.model.Bar;

public class NearbarsActivity extends AppCompatActivity {

    private DatabaseReference reff;
    private String cityName;
    private ArrayList<Bar> bars;
    private ArrayList<String> names;
    private ArrayList<String> barsInCity;
    private ListView listView;
    private int counter = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearbars);

        barsInCity = new ArrayList<>();
        listView = (ListView) findViewById(R.id.citybars);
        bars = new ArrayList<>();
        cityName = getIntent().getStringExtra("barname");
        reff = FirebaseDatabase.getInstance().getReference().child("Bar");

        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    HashMap<String, Object> dataMap = (HashMap<String, Object>) dataSnapshot.getValue();
                    for (String key : dataMap.keySet()) {
                        Object data = dataMap.get(key);
                        try {
                            HashMap<String, Object> userData = (HashMap<String, Object>) data;
                            Bar bar = new Bar((Long) userData.get("id"), (String) userData.get("name"),
                                    (String) userData.get("type"), (String) userData.get("city"), (String) userData.get("street"),
                                    (String) userData.get("text"), (String) userData.get("beer"), (boolean) userData.get("kitchen"),
                                    (boolean) userData.get("garden"));
                            bars.add(bar);
                        } catch (ClassCastException cce) {
                        }
                    }
                    for (Bar bar : bars) {
                        if (bar.getCity().equals(cityName)) {
                            barsInCity.add(bar.getName());
                            counter++;
                        }
                    }
                    ArrayAdapter adapter = new ArrayAdapter(NearbarsActivity.this, android.R.layout.simple_list_item_1, barsInCity);
                    listView.setAdapter(adapter);
                    if (counter == 0) {
                        Toast.makeText(NearbarsActivity.this, "Žádné bary ve Vašem měste!", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = barsInCity.get(position).toString();
                Intent intent = new Intent(getBaseContext(), DetailbarActivity.class);
                intent.putExtra("barname", name);
                startActivity(intent);
            }
        });

        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
