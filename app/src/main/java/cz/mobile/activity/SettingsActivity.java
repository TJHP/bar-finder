package cz.mobile.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import cz.mobile.R;

public class SettingsActivity extends AppCompatActivity {

    private Switch gpsSWI, shakin;
    public SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);


        sharedPreferences = this.getSharedPreferences("SharedPrefGPS", MODE_PRIVATE);

        boolean changeGPS = sharedPreferences.getBoolean("GPS", false);
        gpsSWI = (Switch) findViewById(R.id.locationTracking);
        gpsSWI.setChecked(changeGPS);


        boolean changeShake = sharedPreferences.getBoolean("SHAKE", false);
        shakin = (Switch) findViewById(R.id.shakin);
        shakin.setChecked(changeShake);



        gpsSWI.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("GPS", isChecked);
                editor.commit();
            }
        });

        shakin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("SHAKE", isChecked);
                editor.commit();
            }
        });


        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpTo(this, new Intent(new Intent(this, MainActivity.class)));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
